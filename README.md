## Intro

This repo creates a Linux image for the [aerc-mail](https://aerc-mail.org/)

The source for this repo is on gitlab https://gitlab.com/aleks001/aerc-mailer

## Build instructions

### Buildah

```
buildah bud --tag aerc:0.2.1 .
```

### docker build
```
docker build --tag aerc:0.2.1 .
```

## run instructions

```
podman run --rm -it localhost/aerc:0.2.1
```

The configs are in `/root/.config/aerc/` this means you can map your config 
local config to this directory.

## images size

```
# podman images
REPOSITORY                 TAG      IMAGE ID       CREATED          SIZE
localhost/aerc             0.2.1    ac254a1da06c   40 minutes ago   875 MB
docker.io/library/golang   buster   be63d15101cb   2 weeks ago      834 MB
```