FROM golang:buster

# https://git.sr.ht/~sircmpwn/aerc/archive/0.2.1.tar.gz
ENV VERSION=0.2.1 \
    URL=https://git.sr.ht/~sircmpwn/aerc/archive

RUN set -x \
  && apt-get -y update \
  && export buildDeps='make git scdoc' \
  && apt-get -y install less $buildDeps \
  && mkdir -p /usr/src/aerc \
  && cd /usr/src/aerc \
  && curl -sSLO ${URL}/${VERSION}.tar.gz \
  && tar xvzf ${VERSION}.tar.gz \
  && cd aerc-0.2.1 \
  && make \
  && make install \
  && rm -rf /go/pkg/mod/[a-z]* \
  && apt-get purge -y --auto-remove $buildDeps \ 
  && rm -rf /var/lib/apt/lists/* \
